/*
 * $Id$
 */
package com.ivstars.astrology;

import junit.framework.TestCase;
import junit.framework.Assert;

import java.util.*;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;
import com.ivstars.astrology.util.DegreeUtil;
import com.ivstars.astrology.util.ChineseCalendarGB;
import org.apache.velocity.tools.generic.NumberTool;

/**
 * TestChartModel
 *
 * @author Fengbo Xie (fengbo.xie at gmail.com)
 *         Everything that has a beginning has an end.
 */
public class TestChartModel extends TestCase {
    protected void setUp() throws Exception {
        super.setUp();
        //Config.init();
    }

    public void testChartModel(){
        ChartModel model = new ChartModel(new Date(),39.92,116.46);
        Assert.assertFalse(model.isCflag());
        double jd=model.getJulianDay();
        Assert.assertTrue(model.isCflag());
        model.setDate(new Date());
        Assert.assertFalse(model.isCflag());
        HousesInfo hi = model.getHousesInfo();
        Assert.assertTrue(model.isCflag());
    }

    public void testVelocity() throws Exception {
        Config.initVelocity();
        ChartModel model = new ChartModel(new Date(),39.92,116.46);
        model.setName("张三");
        String[] positions = model.getHousesPosition();
        VelocityContext context = new VelocityContext();
        context.put("numberTool", new NumberTool());
        context.put("julianDay",model.getJulianDay());
        context.put("latitude",model.getLatitude());
        context.put("longitude",model.getLongitude());
        context.put("houses",model.getHousesPosition());
        context.put("planets",model.getPlanets());

        ChartModel model2 = new ChartModel("1980-08-02 6:00:00","+8",35.12,111.22);
        model2.setName("B") ;

        model.setModel2(model2);
        //double delta = model.getHousesInfo().getAscendant() - model2.getHousesInfo().getAscendant();
        //context.put("degreeUtil", new DegreeUtil(delta));
        context.put("extPlanets",model2.getPlanets());

        List<ChartOptions.Aspect> aspects = model.relations(model2);
        context.put("aspects",aspects);
        try {
            Template template = Velocity.getTemplate("astrology.xml.vm");
            Writer writer = new PrintWriter(System.out);
            template.merge(context,writer);
            writer.flush();
        } catch (Exception e) {
            Assert.fail(e.toString());
        }
    }
    public void testJson() throws Exception {
        Config.initVelocity();
        ChartModel model = new ChartModel(new Date(),39.92,116.46);
        model.setName("张三");
        String[] positions = model.getHousesPosition();
        VelocityContext context = new VelocityContext();
        context.put("numberTool", new NumberTool());
        context.put("julianDay",model.getJulianDay());
        context.put("latitude",model.getLatitude());
        context.put("longitude",model.getLongitude());
        context.put("houses",model.getHousesPosition());
        context.put("planets",model.getPlanets());
        context.put("aspects",model.relations()); 
        try {
            Template template = Velocity.getTemplate("astrology.json.vm");
            Writer writer = new PrintWriter(System.out);
            template.merge(context,writer);
            writer.flush();
        } catch (Exception e) {
            Assert.fail(e.toString());
        }
    }
    public void testInfo() throws Exception{
        Config.initVelocity();
        ChartModel model = new ChartModel(new Date(),39.92,116.46);
        model.setName("张三");
        StringRender render = new StringRender();         
        System.out.println(render.render(model));
    }

    public void testRelations() throws Exception{
        Config.initVelocity();
        ChartModel model = new ChartModel("1980-04-10 17:00:00","+8",28.17,113.63);
        model.setName("A");
         StringRender render = new StringRender();         
        System.out.println(render.render(model));
        ChartModel model2 = new ChartModel("1980-08-02 6:00:00","+8",35.12,111.22);
        model2.setName("B") ;        
        System.out.println(render.render(model2));
        double asc = model.getHousesInfo().getAscendant();
        System.out.println("Asc A:"+asc) ;
        double asc2 = model2.getHousesInfo().getAscendant();
        System.out.println("Asc B:"+asc2);
        double delta = asc - asc2;
        System.out.println("delta:"+delta);
        List<ChartOptions.Aspect> aspects = model.relations(model2);
        for(ChartOptions.Aspect aspect: aspects){
            System.out.println(aspect);
            PlanetInfo planetA = aspect.planetA;
            PlanetInfo planetB = aspect.planetB;
            System.out.println(planetA.getPlanetName()+"\t"+planetA.getLongitude()+"\t"+planetA.getTransferedLongitude(asc));
            System.out.println(planetB.getPlanetName()+"\t"+planetB.getLongitude()+"\t"+(planetB.getTransferedLongitude(asc2)-delta));
        }

    }

    public void testProgress() throws Exception{
        Config.initVelocity();
        ChartModel model = new ChartModel("1980-04-10 17:00:00","+8",28.17,113.63);
        model.setName("A");
        model.setProgressModel("2013-09-05 17:00:00","+8",39.93,116.39);
        ChartModel model2 = model.getModel2();
        ChartModel model3 = new ChartModel("2013-09-05 17:00:00","+8",39.93,116.39);
        model3.setName("B");
        StringRender render = new StringRender();
        System.out.println(render.render(model));
        model2.setName("Progress") ;
        System.out.println(render.render(model2));
        double asc = model.getHousesInfo().getAscendant();

        System.out.println("Asc A:"+asc) ;
        double asc2 = model2.getHousesInfo().getAscendant();
        System.out.println("Asc P:"+asc2);
        System.out.println("Asc B:"+model3.getHousesInfo().getAscendant());
        double delta = asc - asc2;
        System.out.println("delta:"+delta);

        System.out.println(render.render(model3));

    }
}
