
      J   AAAAA    SSSSS   TTTTTTT  RRRRRR    OOOOO   L         OOOOO    GGGGG
      J  A     A  S     S     T     R     R  O     O  L        O     O  G     G
      J  A     A  S           T     R     R  O     O  L        O     O  G
      J  AAAAAAA   SSSSS      T     RRRRRR   O     O  L        O     O  G  GGGG
      J  A     A        S     T     R   R    O     O  L        O     O  G     G
      J  A     A  S     S     T     R    R   O     O  L        O     O  G     G
JJJJJJ   A     A   SSSSS      T     R     R   OOOOO   LLLLLLL   OOOOO    GGGGG

                            **  VERSION 0.2  **
/* 
 * Jastrology - Copyright 2006 - 2013 Fengbo Xie.
 *
 * http://sourceforge.net/projects/jastrology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

[INTRODUCTION]
Jastrology is an astrology program written in Java. It provides a Swing UI for desktop interactive, and this package can also be used in an J2EE web application to produce dynamic horoscope diagrams.It uses Swiss Ephemeris package of Astrodienst AG Zuerich for ephemeris calculation. Typically, the Java port of Swiss Ephemeris package from Thomas Mack is been used. You can download this package at http://th-mack.de/international/download with some regularity.
See http://www.astro.ch/swisseph for more information and documentation of Swiss Ephemeris.

The source code is hosted at http://sourceforge.net/projects/jastrology

This product includes software developed by
The Apache Software Foundation (http://www.apache.org/).

[HOW TO RUN]
You can using the script file under the installation directory to launch the program.
For Windows, the script file is jastrology.bat and jastrology.sh for *nix.
If you're familiar with Java program, you can launch the application manually.
The classpath should include classes folder and all jar files under lib folder.
Main class is com.ivstars.astrology.gui.Chart.

[BUG REPORTS]
For bug reports, write email to me <fengbo.xie@gmail.com>.

[Online Manual]
http://fengbo.xie.googlepages.com/home2