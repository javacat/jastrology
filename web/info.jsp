<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
     <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>JAstrology WebApp</title>
    <style>
    </style>
</head>
<body>
    <ul>
        <li>jastrology version: <%=com.ivstars.astrology.Config.getVersionInfo()%></li>
        <li>JVM: <%=System.getProperty("java.vendor")%> <%=System.getProperty("java.vm.name")%> <%=System.getProperty("java.version")%></li>
        <li>OS: <%=System.getProperty("os.name")%> <%=System.getProperty("os.version")%> <%=System.getProperty("os.arch")%></li>
    </ul>
    Generated at :<%=new java.util.Date()%>
</body>
</html>