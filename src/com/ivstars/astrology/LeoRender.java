package com.ivstars.astrology;

import com.ivstars.astrology.util.DegreeUtil;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.geom.Line2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LeoRender extends ImageRender {
    private  static Log log= LogFactory.getLog(LeoRender.class);
	public LeoRender() {
		this("/88astro.properties");
	}

	public LeoRender(String config) {
		super(config);
	}

	public String getName(){
                return this.getConfig().getString("name");
            }

    protected void drawConstellations(ChartModel model) {
        HousesInfo hi = model.getHousesInfo();
        Config cfg = getConfig();
        float r1=cfg.getFloat("r1",0.95f);
        if(r1<1.0)
            r1=cx*r1;
        float r2=cfg.getFloat("r2",0.8f);
        if(r2<1.0)
            r2=cx*r2;
        float r3=cfg.getFloat("r3",0.75f);
        if(r3<1.0)
            r3=cx*r3;
        float r4=cfg.getFloat("r4",0.65f);
        if(r4<1.0)
            r4=cx*r4;
        Color basecolor=cfg.getColor("base");
        Color weakcolor=cfg.getColor("weak");
        double asc=hi.getAscendant();
        Font basefont = cfg.getFont("base"); //new Font("Dialog",Font.PLAIN,8);
        Image img=getConfig().getImage("houseimage");
        float offset = cfg.getFloat("houseimage.offset",0.0f);
        double a0 = DegreeUtil.d2R(DegreeUtil.transfer(offset,asc));
        //double a0 = DegreeUtil.d2R(DegreeUtil.transfer(0,asc));
        AffineTransform at = AffineTransform.getRotateInstance(0-a0,cx,cy);
        g.transform(at);
        log.info("width: "+width+" ratio:"+ratio);
        int yoffset = (int)(height-width*ratio)/2 ;
        log.debug("y offset:"+yoffset);
        g.drawImage(img,0,yoffset,(int)width,(int)(width*ratio),null);
        at = AffineTransform.getRotateInstance(a0,cx,cy);
        g.transform(at);
        //this.drawOval(r1,normalStroke,basecolor);
        //this.drawOval(r2,normalStroke,basecolor);
       // this.drawOval(r3,normalStroke,basecolor);
        //this.drawOval(r4,normalStroke,basecolor);


        //draw ascendant line
        Line2D line = new Line2D.Float(0.0f,cy,width,cy);
        this.draw(line,normalStroke,basecolor);
        //draw mc-ic line
        double mc= DegreeUtil.transfer(hi.getMc(),asc);
        this.drawLine(cx,0-cx,mc,normalStroke,basecolor);
        //draw polluxs
        String name;
        float r12 = (r1+r2)/2-10;
        if(!cfg.getBoolean("ignore.pollux")){
        for(int i=0;i<Constellation.POLLUXS.length;i++){
            name = Constellation.POLLUXS[i];
           double angle=DegreeUtil.transfer(30*i,asc);
            //draw polluxs line
            this.drawLine(r1,r3,angle,normalStroke,basecolor);
            //draw pollux name
            
            BufferedImage hsimg=cfg.getImage("c"+i);
            if(hsimg==null){
                this.drawString(name,r12,angle+15,cfg.getColor(name),basefont);
            }else{
                 this.drawImage(hsimg,r12,angle+15);
                 BufferedImage cpimg=cfg.getImage("cp"+i);
                 if(cpimg !=null){
                     this.drawImage(cpimg, r12, angle+7.5);
                 }
            }
            
            //draw degree lines
            
            for(int j=1;j<30;j++){
                if(j%5 == 0){
                    this.drawLine(r2,r3,angle+j,normalStroke,basecolor);
                }else{
                    this.drawLine(r2,r3,angle+j,halfStroke,weakcolor);
                }
            }
            
        }
        }

        //draw houses
        Font houseFont = cfg.getFont("house");
        HousesInfo thi = hi.transfer();
        float r34=(r3+r4)/2-10;
        for(int i=1;i<13;i++){
            double angle=thi.get(i);
            double angle2;
            if(i<12)
            angle2=thi.get(i+1);
            else angle2=thi.get(1);
            double delta=angle2-angle;
            delta =DegreeUtil.fixAngle(delta);
            this.drawLine(r3,r4,angle,normalStroke,basecolor);
            if(i%3!=1){
                this.drawLine(r4,0,angle,dashStroke,weakcolor);
            }
            //draw house index
             BufferedImage nimg=cfg.getImage("n"+i);
             if(nimg==null){
                this.drawString(Integer.toString(i),(r3+r4)/2,angle+delta/2,cfg.getColor(Constellation.POLLUXS[i-1]),houseFont);
             }else{
                 this.drawImage(nimg, r34, angle+delta/2);
                 BufferedImage npimg=cfg.getImage("np"+i);
                 if(npimg!=null){
                     this.drawImage(npimg,r34,angle+delta/4);
                 }
             }
         }
    }
    /*
    public Object render(ChartModel model) {
        Dimension size = model.getSize();
        int height = getConfig().getInt("height",size.height);
        int width = getConfig().getInt("width",size.width);
        BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();
        render(model,g,width,height);
        g.dispose();
        //uncomment the following code to scale iamge
        if(size.width!=width||size.height!=height){
            image = getScaledImage(image,size.width,size.height);
        }
        return image;
    } */
    public static BufferedImage getScaledImage(BufferedImage image, int width, int height) {
        int imageWidth  = image.getWidth();
        int imageHeight = image.getHeight();

        double scaleX = (double)width/imageWidth;
        double scaleY = (double)height/imageHeight;
        AffineTransform scaleTransform = AffineTransform.getScaleInstance(scaleX, scaleY);
        AffineTransformOp bilinearScaleOp = new AffineTransformOp(scaleTransform, AffineTransformOp.TYPE_BILINEAR);

        return bilinearScaleOp.filter(
            image,
            new BufferedImage(width, height, image.getType()));
    }
}
